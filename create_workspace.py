import copy
import sys
import signac
import itertools
import numpy as np
from collections import OrderedDict


def get_gen_parameters():
    parameters = OrderedDict()
    # Generate Parameters
    parameters["dimensions"] = ["10x10x1"]
    parameters["template"] = ["M1UnitCell.pdb"]
    parameters["reactant_density"] = [0.005,0.01,0.05]
    parameters["stoichiometry"] = ["{'Nb':0.13,'Te':0.12,'Mo':1,'V':0.15}"]
    parameters["forcefield"] = ["FF_opls_uff.xml"]  
    parameters["crystal_x"] = ["2.148490"]
    parameters["crystal_y"] = ["2.664721"]
    parameters["crystal_z"] = ["0.400321"]
    parameters["job_type"] = ["parent"]
    return list(parameters.keys()), list(itertools.product(*parameters.values()))


def get_sim_parameters():
    parameters = OrderedDict()
    # Simulate Parameters
    parameters["temperature"] = [300,500,700]
    parameters["run_time"] = [1E7]
    return list(parameters.keys()), list(itertools.product(*parameters.values()))


if __name__ == "__main__":
    project = signac.init_project("Neale_Test")
    gen_param_names, gen_param_combinations = get_gen_parameters()
    sim_param_names, sim_param_combinations = get_sim_parameters()
    # Create the generate jobs
    for gen_params in gen_param_combinations:
        parent_statepoint = dict(zip(gen_param_names, gen_params))
        parent_job = project.open_job(parent_statepoint)
        parent_job.init()
        for sim_params in sim_param_combinations:
            child_statepoint = copy.deepcopy(parent_statepoint)
            child_statepoint.update(dict(zip(sim_param_names, sim_params)))
            child_statepoint["parent_statepoint"] = parent_job.statepoint()
            project.open_job(child_statepoint).init()
    project.write_statepoints()
